package iox

import (
	"errors"
	"fmt"
	"io"
)

var (

	//> To ensure interface compliance
	_ io.Writer      = NewBytesWriteSeeker()
	_ io.Seeker      = NewBytesWriteSeeker()
	_ io.WriteSeeker = NewBytesWriteSeeker()
)

type BytesWriteSeeker struct {
	buf      []byte
	end, pos int
	posStack []int
}

const defaultBytesWriteSeekerBufferSize = 1024 * 4

func NewBytesWriteSeeker() *BytesWriteSeeker {
	return NewBytesWriteSeekerSize(defaultBytesWriteSeekerBufferSize)
}

func NewBytesWriteSeekerSize(size int) *BytesWriteSeeker {
	return &BytesWriteSeeker{
		buf: make([]byte, size),
	}
}

func (this *BytesWriteSeeker) Reset() {
	this.end = 0
	this.pos = 0
	this.posStack = []int{}
}

func (this *BytesWriteSeeker) Buffer() []byte {
	return this.buf[:this.Size()]
}

func (this *BytesWriteSeeker) Size() int {
	return this.end
}

func (this *BytesWriteSeeker) Cap() int {
	return cap(this.buf)
}

func (this *BytesWriteSeeker) GrowToFit(size int) {
	for size > len(this.buf) {
		this.buf = append(this.buf, make([]byte, len(this.buf))...)
	}
}

func (this *BytesWriteSeeker) Write(p []byte) (n int, err error) {
	//> Grow buffer if necessary
	this.GrowToFit(this.pos + len(p))

	//> Copy data
	n = copy(this.buf[this.pos:], p)

	if n != len(p) {
		panic(fmt.Sprintf("Mismatched bytes wrote count, should be %d but got %d", len(p), n))
	}

	this.pos += len(p)

	if this.pos > this.end {
		this.end = this.pos
	}

	return
}

func (this *BytesWriteSeeker) Seek(offset int64, whence int) (newOffset int64, err error) {
	if offset < 0 {
		panic("Offset must be a positive value")
	}
	switch whence {
	case SeekOrigin:
		this.pos = int(offset)
	case SeekCurrent:
		this.pos += int(offset)
	case SeekEnd:
		this.pos = this.end + int(offset)
	default:
		err = errors.New("Invalid parameter: whence")
	}
	newOffset = int64(this.pos)
	return
}

func (this *BytesWriteSeeker) Rewind() (err error) {
	_, err = this.Seek(SeekOrigin, 0)
	return
}

func (this *BytesWriteSeeker) Pos() int {
	return this.pos
}

func (this *BytesWriteSeeker) PosPush() {
	this.posStack = append(this.posStack, this.pos)
}

func (this *BytesWriteSeeker) PosPop() {
	size := len(this.posStack)
	if size > 0 {
		this.pos = this.posStack[size-1]
		this.posStack = this.posStack[:size-1]
	}
}

func (this *BytesWriteSeeker) TrimBuffer() {
	buf := make([]byte, this.Size())
	copy(buf, this.buf)
	this.buf = buf
}

// todo: TrimCopy func
// todo: PopFront func
// todo: PushFront func

//// WRONG
//func (this *BytesWriteSeeker) BufferFromPos(bytesRequired int) []byte {
//	this.GrowTo(this.Pos() + 1 + bytesRequired)
//	return this.Buffer()[this.Pos():]
//}
