package iox

import (
	"bufio"
	"io"
)

type CountingBufioWriter struct {
	bw         *bufio.Writer
	bytesWrote int
}

func NewCountingBufioWriter(w io.Writer) *CountingBufioWriter {
	return &CountingBufioWriter{
		bw: bufio.NewWriter(w),
	}
}

func (this *CountingBufioWriter) BytesWrote() int {
	return this.bytesWrote
}

func (this *CountingBufioWriter) ResetWrote() {
	this.bytesWrote = 0
}

func (this *CountingBufioWriter) Reset(w io.Writer) {
	this.bw.Reset(w)
	this.bytesWrote = 0
}

func (this *CountingBufioWriter) Flush() error {
	return this.bw.Flush()
}

func (this *CountingBufioWriter) Available() int {
	return this.bw.Available()
}

func (this *CountingBufioWriter) Buffered() int {
	return this.bw.Buffered()
}

func (this *CountingBufioWriter) Write(p []byte) (nn int, err error) {
	nn, err = this.bw.Write(p)
	this.bytesWrote += nn
	return
}

func (this *CountingBufioWriter) WriteByte(c byte) error {
	err := this.bw.WriteByte(c)
	if err == nil {
		this.bytesWrote += 1
	}
	return err
}

func (this *CountingBufioWriter) WriteRune(r rune) (size int, err error) {
	size, err = this.bw.WriteRune(r)
	this.bytesWrote += size
	return
}

func (this *CountingBufioWriter) WriteString(s string) (nn int, err error) {
	nn, err = this.bw.WriteString(s)
	this.bytesWrote += nn
	return
}

func (this *CountingBufioWriter) ReadFrom(r io.Reader) (n int64, err error) {
	n, err = this.bw.ReadFrom(r)
	this.bytesWrote += int(n)
	return
}
