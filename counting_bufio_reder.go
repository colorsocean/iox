package iox

import (
	"bufio"
	"io"
)

type CountingBufioReader struct {
	br        *bufio.Reader
	bytesRead int
}

func NewCountingBufioReader(r io.Reader) *CountingBufioReader {
	return &CountingBufioReader{
		br: bufio.NewReader(r),
	}
}

func (this *CountingBufioReader) BytesRead() int {
	return this.bytesRead
}

func (this *CountingBufioReader) ResetRead() {
	this.bytesRead = 0
}

func (this *CountingBufioReader) Reset(r io.Reader) {
	this.br.Reset(r)
	this.ResetRead()
}

// Peek returns the next n bytes without advancing the reader. The bytes stop
// being valid at the next read call. If Peek returns fewer than n bytes, it
// also returns an error explaining why the read is short. The error is
// ErrBufferFull if n is larger than b's buffer size.
func (this *CountingBufioReader) Peek(n int) (b []byte, err error) {
	return this.br.Peek(n)
}

func (this *CountingBufioReader) Read(p []byte) (n int, err error) {
	n, err = this.br.Read(p)
	this.bytesRead += n
	return
}

func (this *CountingBufioReader) ReadByte() (c byte, err error) {
	c, err = this.br.ReadByte()
	if err == nil {
		this.bytesRead += 1
	}
	return
}

// UnreadByte unreads the last byte.  Only the most recently read byte can be unread.
func (this *CountingBufioReader) UnreadByte() error {
	err := this.br.UnreadByte()
	if err == nil {
		this.bytesRead -= 1
	}
	return err
}

func (this *CountingBufioReader) ReadRune() (r rune, size int, err error) {
	r, size, err = this.br.ReadRune()
	this.bytesRead += size
	return
}

// UnreadRune unreads the last rune.  If the most recent read operation on
// the buffer was not a ReadRune, UnreadRune returns an error.  (In this
// regard it is stricter than UnreadByte, which will unread the last byte
// from any read operation.)
func (this *CountingBufioReader) UnreadRune() error {
	return this.br.UnreadRune()
}

//// Buffered returns the number of bytes that can be read from the current buffer.
//func (this *CountingBufioReader) Buffered() int { return b.w - b.r }

//// ReadSlice reads until the first occurrence of delim in the input,
//// returning a slice pointing at the bytes in the buffer.
//// The bytes stop being valid at the next read.
//// If ReadSlice encounters an error before finding a delimiter,
//// it returns all the data in the buffer and the error itself (often io.EOF).
//// ReadSlice fails with error ErrBufferFull if the buffer fills without a delim.
//// Because the data returned from ReadSlice will be overwritten
//// by the next I/O operation, most clients should use
//// ReadBytes or ReadString instead.
//// ReadSlice returns err != nil if and only if line does not end in delim.
//func (this *CountingBufioReader) ReadSlice(delim byte) (line []byte, err error) {
//}

//// ReadLine is a low-level line-reading primitive. Most callers should use
//// ReadBytes('\n') or ReadString('\n') instead or use a Scanner.
////
//// ReadLine tries to return a single line, not including the end-of-line bytes.
//// If the line was too long for the buffer then isPrefix is set and the
//// beginning of the line is returned. The rest of the line will be returned
//// from future calls. isPrefix will be false when returning the last fragment
//// of the line. The returned buffer is only valid until the next call to
//// ReadLine. ReadLine either returns a non-nil line or it returns an error,
//// never both.
////
//// The text returned from ReadLine does not include the line end ("\r\n" or "\n").
//// No indication or error is given if the input ends without a final line end.
//// Calling UnreadByte after ReadLine will always unread the last byte read
//// (possibly a character belonging to the line end) even if that byte is not
//// part of the line returned by ReadLine.
//func (this *CountingBufioReader) ReadLine() (line []byte, isPrefix bool, err error) {
//}

//// ReadBytes reads until the first occurrence of delim in the input,
//// returning a slice containing the data up to and including the delimiter.
//// If ReadBytes encounters an error before finding a delimiter,
//// it returns the data read before the error and the error itself (often io.EOF).
//// ReadBytes returns err != nil if and only if the returned data does not end in
//// delim.
//// For simple uses, a Scanner may be more convenient.
//func (this *CountingBufioReader) ReadBytes(delim byte) (line []byte, err error) {
//}

//// ReadString reads until the first occurrence of delim in the input,
//// returning a string containing the data up to and including the delimiter.
//// If ReadString encounters an error before finding a delimiter,
//// it returns the data read before the error and the error itself (often io.EOF).
//// ReadString returns err != nil if and only if the returned data does not end in
//// delim.
//// For simple uses, a Scanner may be more convenient.
//func (this *CountingBufioReader) ReadString(delim byte) (line string, err error) {
//}

//// WriteTo implements io.WriterTo.
//func (this *CountingBufioReader) WriteTo(w io.Writer) (n int64, err error) {
//}
