package iox

import (
	"io"
)

var _ io.Writer = new(CountingWriter)

type CountingWriter struct {
	w     io.Writer
	count int
}

func NewCountingWriter(w io.Writer) *CountingWriter {
	return &CountingWriter{
		w:     w,
		count: 0,
	}
}

func (this *CountingWriter) Write(p []byte) (n int, err error) {
	n, err = this.w.Write(p)
	this.count += n
	return
}

func (this *CountingWriter) WriteCount() int {
	return this.count
}

func (this *CountingWriter) WriteReset() (n int) {
	n = this.count
	this.count = 0
	return
}
