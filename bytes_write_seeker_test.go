package iox

import (
	"bytes"
	"encoding/binary"
	"testing"
)

func fberror(b *testing.B, err error) {
	if err != nil {
		b.FailNow()
	}
}

func BenchmarkBytesWriteSeeker_Int64(b *testing.B) {
	var err error
	buf := NewBytesWriteSeeker()

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		err = binary.Write(buf, binary.BigEndian, int64(1234))
		fberror(b, err)
	}
	b.StopTimer()
}

func BenchmarkBytesBuffer_Int64(b *testing.B) {
	var err error
	buf := bytes.NewBuffer([]byte{})

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		err = binary.Write(buf, binary.BigEndian, int64(1234))
		fberror(b, err)
	}
	b.StopTimer()
}

func BenchmarkBytesWriteSeeker_Int64_Bytes100(b *testing.B) {
	var err error
	buf := NewBytesWriteSeeker()
	data1 := make([]byte, 100)

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		_, err = buf.Write(data1)
		fberror(b, err)

		err = binary.Write(buf, binary.BigEndian, int64(1234))
		fberror(b, err)
	}
	b.StopTimer()

	b.Log("Cap:", buf.Cap())
}

func BenchmarkBytesWriteSeeker_Int64_Bytes100_Seek(b *testing.B) {
	var err error
	buf := NewBytesWriteSeeker()
	data1 := make([]byte, 100)

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		_, err = buf.Write(data1)
		fberror(b, err)

		_, err = buf.Seek(int64(buf.Pos()-50), SeekOrigin)
		fberror(b, err)

		err = binary.Write(buf, binary.BigEndian, int64(1234))
		fberror(b, err)

		_, err = buf.Seek(0, SeekEnd)
		fberror(b, err)
	}
	b.StopTimer()

	b.Log("Cap:", buf.Cap())
}
